package com.example.text_to_speechtts;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {
    final static int INTENT_CHECK_TTS = 0;
    double speechRate =0, pitchRate = 0;
    TextToSpeech tts;
    EditText edt;
    Button btn;
    SeekBar sbSpeechRate, sbPitchRate;
    TextView tvSpeechRate, tvPitchRate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Intent intent = new Intent();
        intent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
        startActivityForResult(intent, INTENT_CHECK_TTS);
        edt = (EditText)findViewById(R.id.editText1);
        btn = (Button)findViewById(R.id.button1);
        sbSpeechRate = (SeekBar) findViewById(R.id.sBSpeechRate);
        sbPitchRate = (SeekBar)findViewById(R.id.sBPitchRate);
        tvSpeechRate = (TextView) findViewById(R.id.textView1);
        tvPitchRate = (TextView) findViewById(R.id.textView2);
        speechRate = ((double)sbSpeechRate.getProgress())/10;
        tvSpeechRate.setText(String.format( "Speech Rate : %.1f", speechRate ));
        pitchRate = ((double)sbPitchRate.getProgress())/10;
        tvPitchRate.setText(String.format( "Pitch Rate : %.1f", pitchRate ));
        sbSpeechRate.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                speechRate = ((double)progress)/10;
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                tvSpeechRate.setText(String.format( "Speech Rate : %.1f", speechRate ));
            }
        });
        sbPitchRate.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                pitchRate = ((double)progress)/10;
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                tvPitchRate.setText(String.format( "Pitch Rate : %.1f", pitchRate ));
            }
        });
        btn.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {
                String str = edt.getText().toString();
                tts.setPitch((float) pitchRate);
                tts.setSpeechRate((float) speechRate);
                tts.speak(str, TextToSpeech.QUEUE_ADD,null,null);
            }
        });
    }
    public void onActivityResult (int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == INTENT_CHECK_TTS) {
            if (resultCode == TextToSpeech.Engine.CHECK_VOICE_DATA_PASS) {
                tts = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
                    @Override
                    public void onInit(int status) {
                        tts.setLanguage(Locale.US);
                    }
                });
            } else {

                Intent intent = new Intent();
                intent.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
                startActivity(intent);
            }
        }
    }
    public void onDestroy(){
        super.onDestroy();
        if (tts != null)
            tts.shutdown();

    }
}





